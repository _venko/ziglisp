const std = @import("std");
const io = std.io;

const Allocator = std.mem.Allocator;
const ArrayList = std.ArrayList;

const editline = @cImport({
    @cInclude("editline/readline.h");
});

const std_in = io.getStdIn();
const std_out = io.getStdOut();
const std_err = io.getStdErr();

const prompt = "> ";
var input: [4096]u8 = [_]u8{0} ** 4096;

fn readLine() ?[]const u8 {
    if (editline.readline(prompt)) |line|
        return std.mem.span(line);
    return null;
}

const Tokenizer = struct {
    pub const Token = enum {
        /// Sentinel value for unrecognized tokens
        unknown,
        eof,
        whitespace,
        lparen,
        rparen,
    };

    /// The text being tokenized.
    text: []const u8,
    /// The current position of the tokenizer in `text`.
    position: usize,
    /// The tokens accumulated so far.
    tokens: ArrayList(Token),

    pub fn init(text: []const u8, alloc: Allocator) !Tokenizer {
        const size = text.len;
        return Tokenizer{
            .text = text,
            .position = 0,
            .tokens = try ArrayList(Token).initCapacity(alloc, size),
        };
    }

    pub fn deinit(self: *Tokenizer) void {
        self.tokens.deinit();
    }

    fn isWhitespace(byte: u8) bool {
        return byte == ' ' or byte == '\t' or byte == '\n';
    }

    /// Reads the next token starting at `position` in `text`.
    pub inline fn nextToken(self: *Tokenizer) Token {
        defer self.position += 1;

        if (self.position == self.text.len) return Token.eof;

        const c = self.text[self.position];
        if (isWhitespace(c)) return Token.whitespace;
        if (c == '(') return Token.lparen;
        if (c == ')') return Token.rparen;
        return Token.unknown;
    }

    pub fn tokenize(self: *Tokenizer) !void {
        var last_seen = self.nextToken();
        while (last_seen != Token.eof) {
            try self.tokens.append(last_seen);
            last_seen = self.nextToken();
        }
    }
};

pub fn main() anyerror!void {
    // IO buffering is probably overkill right now but might as well have this for reference.
    var out_buf = io.bufferedWriter(std_out.writer());
    var writer = out_buf.writer();

    try writer.print("Ziglisp 0.0.1\n", .{});
    try writer.print("Press Ctrl-C to exit.\n\n", .{});
    try out_buf.flush();

    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();

    while (true) {
        if (readLine()) |line| {
            var tokenizer = try Tokenizer.init(line, allocator);
            _ = try tokenizer.tokenize();
            try writer.print("Tokens: {s}\n", .{tokenizer.tokens.items});
            try out_buf.flush();
            tokenizer.deinit();
        }
    }
}
