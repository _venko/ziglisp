# Ziglisp (WIP)
Decided to try working my way through [Build Your Own Lisp](https://www.buildyourownlisp.com/) using [Zig](https://ziglang.org/).

Currently targeting Zig 0.9.1.
